"""
cxfreeze build script for Cinema_Qt-Viewer

Ubuntu-12.04.5 (tested with cx_Freeze 4.0.1)
-------------------------------------------
Currently not working, cx_Freeze has issues iterating over the paths ('NoneType' object
is not iterable). Use command line tool instead:

	$  cxfreeze --default-path=/usr/local/lib/python2.7/dist-packages/:/usr/lib/python2.7:/home/alvaro/workspace/source/cinema_python/ --include-modules=PySide.QtCore,PySide.QtGui,cinema_python,encodings.hex_codec,numpy --include-path=/usr/lib/python2.7/dist-packages  Cinema.py

	@Note There is no parameter available in the command line tool to pass an icon.
	
Windows 7 - x64 (tested with cx_Freeze 4.3.4)	
---------------------------------------------
Installed all packages with Anaconda.  Script works, run from within the anaconda environment:

	> ipython setup_cxfreeze.py build
	
@Note In either case, builtin_tables.json needs to be copied manually to the executable directory for the
application to run.
"""

import sys
from cx_Freeze import setup, Executable
import os


# test again in Ubuntu after the meaningful error messages that helped fix the script using cxfreeze 4.3.4

# Options:  http://cx-freeze.readthedocs.org/en/latest/distutils.html#build-exe
build_exe_options = { "includes" : ["PySide.QtCore", "PySide.QtGui", "cinema_python", "encodings.hex_codec", "numpy"],
                      #"path" : ["/usr/lib/python2.7", "/usr/local/lib/python2.7/dist-packages", "/usr/lib/python2.7/dist-packages", "/home/alvaro/workspace/source/cinema_python"],
                      #"replace_paths" : [],
                      #"packages" : [],
                      #"excludes" : [],
                      "icon" : "cinema_logo_icon_black.ico"}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(  name = "Cinema",
        version = "2",
        description = "Viewer for Cinema databases",
        options = {"build_exe": build_exe_options},
        executables = [Executable("Cinema.py", base=base)])
